import java.util.Random;

public class Ship {
    private int size;
    public static final int DIRECTION_HOR = 0;
    public static final int DIRECTION_VER = 1;
    public static final String DECK = "d";
    public static final String EMPTY_CELL = "-";
    public static final String CLOSED_CELL = "n";

    String [][] ships = new  String[11][11];

    public Ship(){}


    public String[][] fillByShip(){

        generateEmptyField(ships);

        for(int sizeShip=4; sizeShip>0;sizeShip--){

            if(sizeShip==4){
                checkOccuppiedCell(sizeShip);
            }

            if(sizeShip==3){
                for(int numShip=1; numShip<=2;numShip++){
                    checkOccuppiedCell(sizeShip);
                }
            }

            if(sizeShip==2){
                for(int numShip=1; numShip<=3;numShip++){
                    checkOccuppiedCell(sizeShip);
                }
            }

            if(sizeShip==1){
                for(int numShip=1; numShip<=4;numShip++){
                    checkOccuppiedCell(sizeShip);
                }
            }
        }

        clearClosedCell(ships);

        return ships;
    }

    public void checkOccuppiedCell(int sizeShip){
        int startX=0;
        int startY=0;
        int dir=0;
        boolean isOccupiedCecll = true;
        Random random = new Random();
        while(isOccupiedCecll ) {
            startX = random.nextInt(10 - sizeShip) + 1;
            startY = random.nextInt(10 - sizeShip) + 1;
            dir = random.nextInt(2);
            int count = 0;

            if (dir== DIRECTION_HOR){
                for(int x = startX; x<startX+sizeShip; x++){
                    if(ships[startY][x].equals(DECK)){
                        count++;
                    }
                    if (ships[startY][x].equals(CLOSED_CELL)){
                        count++;
                    }
                }
                if(count>0) continue ;
            }

            if (dir== DIRECTION_VER){
                for(int y = startY; y<startY+sizeShip; y++){
                    if(ships[y][startX].equals(DECK)){
                        count++;
                    }
                    if(ships[y][startX].equals(CLOSED_CELL)){
                        count++;
                    }
                }
                if(count>0) continue ;
            }
            isOccupiedCecll = false;
            genShip(sizeShip,startX,startY,dir);
        }
    }

    private void genShip(int sizeShip, int startX, int startY, int dir){

        if(dir == DIRECTION_HOR){
            for (int d = startX; d < startX+sizeShip; d++){
                ships[startY][d] = DECK;
            }
            if (startY == 1 ){
                if ((startX+(sizeShip-1)) == 10){
                    ships[startY][startX-1] = CLOSED_CELL;
                    for (int d = startX-1; d < startX+sizeShip; d++){

                        ships[startY+1][d]= CLOSED_CELL;
                    }
                } else if(startX == 1){
                    ships[startY][startX+sizeShip] = CLOSED_CELL;
                    for (int d = startX; d <= startX+sizeShip; d++){
                        ships[startY+1][d]= CLOSED_CELL;
                    }
                }else {
                    ships[startY][startX-1] = CLOSED_CELL;
                    ships[startY][startX+sizeShip] = CLOSED_CELL;
                    for (int d = startX-1; d <= startX+sizeShip; d++){
                        ships[startY+1][d]= CLOSED_CELL;
                    }
                }
            } else if(startY == 10){
                if ((startX+(sizeShip-1)) == 10){
                    ships[startY][startX-1] = CLOSED_CELL;
                    for (int d = startX-1; d < startX+sizeShip; d++){
                        ships[startY-1][d]= CLOSED_CELL;
                    }
                } else if(startX == 1){
                    ships[startY][startX+sizeShip] = CLOSED_CELL;
                    for (int d = startX; d <= startX+sizeShip; d++){
                        ships[startY-1][d]= CLOSED_CELL;
                    }
                }else {
                    ships[startY][startX-1] = CLOSED_CELL;
                    ships[startY][startX+sizeShip] = CLOSED_CELL;
                    for (int d = startX-1; d <= startX+sizeShip; d++){
                        ships[startY-1][d]= CLOSED_CELL;
                    }
                }

            }else {
                if ((startX+(sizeShip-1)) == 10){
                    ships[startY][startX-1] = CLOSED_CELL;
                    for (int d = startX-1; d < startX+sizeShip; d++){
                        ships[startY+1][d]= CLOSED_CELL;
                        ships[startY-1][d]= CLOSED_CELL;
                    }
                } else if(startX == 1){
                    ships[startY][startX+sizeShip] = CLOSED_CELL;
                    for (int d = startX; d <= startX+sizeShip; d++){
                        ships[startY+1][d]= CLOSED_CELL;
                        ships[startY-1][d]= CLOSED_CELL;
                    }
                }else {
                    ships[startY][startX-1] = CLOSED_CELL;
                    ships[startY][startX+sizeShip] = CLOSED_CELL;
                    for (int d = startX-1; d <= startX+sizeShip; d++){
                        ships[startY+1][d]= CLOSED_CELL;
                        ships[startY-1][d]= CLOSED_CELL;
                    }
                }

            }

        }
        if (dir == DIRECTION_VER) {
            for (int d = startY; d < startY + sizeShip; d++) {
                ships[d][startX] = DECK;
            }
            if (startX == 1) {
                if ((startY + (sizeShip - 1)) == 10) {
                    ships[startY - 1][startX] = CLOSED_CELL;
                    for (int d = startY - 1; d < startY + sizeShip; d++) {
                        ships[d][startX + 1] = CLOSED_CELL;
                    }
                } else if (startY == 1) {
                    ships[startY + sizeShip][startX] = CLOSED_CELL;
                    for (int d = startY; d <= startY + sizeShip; d++) {
                        ships[d][startX + 1] = CLOSED_CELL;
                    }
                } else {
                    ships[startY - 1][startX] = CLOSED_CELL;
                    ships[startY + sizeShip][startX] = CLOSED_CELL;
                    for (int d = startY - 1; d <= startY + sizeShip; d++) {
                        ships[d][startX + 1] = CLOSED_CELL;
                    }
                }
            } else if (startX == 10) {
                if ((startY + (sizeShip - 1)) == 10) {
                    ships[startY - 1][startX] = CLOSED_CELL;
                    for (int d = startY - 1; d < startY + sizeShip; d++) {
                        ships[d][startX - 1] = CLOSED_CELL;
                    }
                } else if (startY == 1) {
                    ships[startY + sizeShip][startX] = CLOSED_CELL;
                    for (int d = startY; d <= startY + sizeShip; d++) {
                        ships[d][startX - 1] = CLOSED_CELL;
                    }
                } else {
                    ships[startY - 1][startX] = CLOSED_CELL;
                    ships[startY + 4][startX] = CLOSED_CELL;
                    for (int d = startY - 1; d <= startY + sizeShip; d++) {
                        ships[d][startX - 1] = CLOSED_CELL;
                    }
                }

            } else {
                if ((startY + (sizeShip - 1)) == 10) {
                    ships[startY - 1][startX] = CLOSED_CELL;
                    for (int d = startY - 1; d < startY + sizeShip; d++) {
                        ships[d][startX + 1] = CLOSED_CELL;
                        ships[d][startX - 1] = CLOSED_CELL;
                    }
                } else if (startY == 1) {
                    ships[startY + sizeShip][startX] = CLOSED_CELL;
                    for (int d = startY; d <= startY + sizeShip; d++) {
                        ships[d][startX + 1] = CLOSED_CELL;
                        ships[d][startX - 1] = CLOSED_CELL;
                    }
                } else {
                    ships[startY - 1][startX] = CLOSED_CELL;
                    ships[startY + sizeShip][startX] = CLOSED_CELL;
                    for (int d = startY - 1; d <= startY + sizeShip; d++) {
                        ships[d][startX + 1] = CLOSED_CELL;
                        ships[d][startX - 1] = CLOSED_CELL;
                    }
                }

            }
        }
    }

    public void prinFieldWhithShips(String [][] ships){
        for (int i=0; i<ships.length; i++){
            for (int j=0; j<ships[i].length;j++){
                System.out.print( ships[i][j] + " | ");
            }
            System.out.println();
        }
    }

    private void generateEmptyField(String[][] ships){
        String [] colum = {" ","A", "B", "C", "D", "E", "F", "G", "H", "I", "J"};

        for (int i=0; i< ships.length; i++){

            for(int j=0; j<ships[i].length; j++){
                if(i==0){
                    if(j==0){
                        ships[i][j] = colum[j].concat(" ");
                    }else {
                        ships[i][j] = colum[j];
                    }
                } else if (j==0){
                    String numberString = Integer.toString(i);
                    if(i<10) {
                        ships[i][j] = numberString.concat(" ");
                    }else {
                        ships[i][j] = numberString;
                    }
                } else {
                    ships[i][j] = EMPTY_CELL;
                }
            }
        }
    }

    private void clearClosedCell(String[][] ships){
        for (int i=0; i< ships.length; i++){
            for(int j=0; j<ships[i].length; j++){
                if(ships[i][j] == "n"){
                    ships[i][j] = EMPTY_CELL;
                }
            }
        }
    }

}
